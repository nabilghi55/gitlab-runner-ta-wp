package docker

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strconv"
	"time"

	"golang.org/x/crypto/ssh"

	"gitlab.com/gitlab-org/gitlab-runner/executors"
	"gitlab.com/gitlab-org/gitlab-runner/helpers"
)

// Get default location of a private key
func privateKeyPath() string {
	return os.Getenv("HOME") + "/.ssh/id_rsa"
}

// Get private key for ssh authentication
func parsePrivateKey(keyPath string) (ssh.Signer, error) {
	buff, _ := ioutil.ReadFile(keyPath)
	return ssh.ParsePrivateKey(buff)
}

// Get ssh client config for our connection
// SSH config will use 2 authentication strategies: by key and by password
func makeSshConfig(user, password string) (*ssh.ClientConfig, error) {
	key, err := parsePrivateKey(privateKeyPath())
	if err != nil {
		return nil, err
	}

	config := ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	return &config, nil
}

// Handle local client connections and tunnel data to the remote server
// Will use io.Copy - http://golang.org/pkg/io/#Copy
func handleClient(client net.Conn, remote net.Conn) {
	defer client.Close()
	defer remote.Close()
	chDone := make(chan bool)

	// Start remote -> local data transfer
	go func() {
		_, err := io.Copy(client, remote)
		if err != nil {
			log.Println("error while copy remote->local:", err)
		}
		chDone <- true
	}()

	// Start local -> remote data transfer
	go func() {
		_, err := io.Copy(remote, client)
		if err != nil {
			log.Println(err)
		}
		chDone <- true
	}()

	<-chDone
	<-chDone
}

func execCommand(cmd string, user string, addr string, retry bool) {
	cfg, err := makeSshConfig(user, "")
	if err != nil {
		log.Fatalln(err)
	}

	for i := 0; i < 10; i++ {
		conn, err := ssh.Dial("tcp", addr, cfg)
		if err != nil {
			log.Fatal(err)
		}

		defer conn.Close()

		sess, err := conn.NewSession()
		if err != nil {
			log.Fatal(err)
		}

		sess.Stdout = os.Stdout
		sess.Stderr = os.Stderr

		err = sess.Run(cmd)
		sess.Close()
		if err == nil || !retry {
			break
		}
		if err != nil {
			time.Sleep(time.Second * 2)
		}
	}
}

func SetupContainerManager(e *executor) (string, func()) {
	projectID := e.Build.JobInfo.ProjectID
	uuid, _ := helpers.GenerateRandomUUID(16)
	name := fmt.Sprintf("runner-%d-%s", projectID, uuid)
	cmd := fmt.Sprintf("create --image ubuntu/18.04/docker %s", name)

	vars := e.Build.GetAllVariables()
	regex := vars.Get("CI_CM_GPU_REGEX")
	count, _ := strconv.Atoi(vars.Get("CI_CM_GPU_COUNT"))

	host, err := executors.GetGPUHost(regex, count)
	if err != nil {
		e.BuildLogger.Errorln(err)
		return "", nil
	}
	sshAddr := fmt.Sprintf("%s.mitre.org:22", host)
	e.BuildLogger.Infoln(name + "@" + sshAddr)
	execCommand(cmd, "containers", sshAddr, false)
	execCommand("docker ps", name, sshAddr, true)

	token := vars.Get("CI_CM_PROJECT_TOKEN")
	if token != "" {
		projects, err := executors.DecryptProjectToken(token)
		if err != nil {
			e.BuildLogger.Errorln("Error loading project token", err)
			return "", nil
		}
		for _, project := range projects {
			cmd := fmt.Sprintf("project attach --project %s %s", project, name)
			execCommand(cmd, "containers", sshAddr, false)
		}
	}

	getCmd := fmt.Sprintf("gpu get --lock %d", count)
	execCommand(getCmd, name, sshAddr, true)

	// Build SSH client configuration
	cfg, err := makeSshConfig(name, "")
	if err != nil {
		log.Fatalln(err)
	}

	// Establish connection with SSH server
	conn, err := ssh.Dial("tcp", sshAddr, cfg)
	if err != nil {
		log.Fatalln(err)
	}

	// Establish connection with remote server

	// Start local server to forward traffic to remote connection
	local, err := net.Listen("unix", "/tmp/"+name)
	if err != nil {
		log.Fatalln(err)
	}

	// Handle incoming connections
	go func() {
		defer conn.Close()
		for {
			client, err := local.Accept()
			if err != nil {
				return
			}

			go func() {
				remote, err := conn.Dial("unix", "/var/run/docker.sock")
				if err != nil {
					panic(err)
				}

				handleClient(client, remote)
			}()
		}
	}()

	cleanup := func() {
		cmd := fmt.Sprintf("rm %s", name)
		execCommand(cmd, "containers", "woolf.mitre.org:22", false)
		local.Close()
	}
	return "unix:///tmp/" + name, cleanup
}
